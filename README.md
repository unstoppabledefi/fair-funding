# Fair Funding

Fair Funding is a concept that allows crypto projects to be funded while limiting the downside exposure of investors.
The Fair Funding platform performs an auction every day (similar to NounsDAO) that promises benefits in the funded project.
Investors bid in the auction and the highest bidder after 24h wins.
The funds deposited in the auction by the investor are then put into an Alchemix vault and a non-liquidatable, self-repaying loan is taken out against this position.
The loan is sent to the raising project while the investor retains complete control over his funds at Alchemix with the help of an ERC721 that represents his position. Over time the self-repaying feature of Alchemix will release the funds and the token owners can claim their share until fully repaid.


# Resources

## About Fair Funding
- [Fair Funding Introduction Article](https://unstoppabledefi.medium.com/fair-funding-in-crypto-bc88d633646)
- [Fair Funding Campaign Article](https://unstoppabledefi.medium.com/fair-funding-campaign-662131dfa3f6)

## Integrations
- [Alchemix Finance](https://alchemix.fi)
- [Alchemix Contracts on Github](https://github.com/alchemix-finance/v2-foundry/tree/master/src)


# On-chain context

```
DEPLOYMENT: Ethereum Mainnet
ERC20: WETH
ERC721: MintableERC721 (part of this audit)
ERC777: none
FEE-ON-TRANSFER: none
REBASING TOKENS: none
ADMIN: trusted
EXTERNAL-ADMINS: trusted
```

# Priviledged Roles
## `AuctionHouse`: 
    1) `owner` can start/stop auction, refund highest bidder if needed and set the target vault contract

## `Vault`: 
    1) `is_operator`: can set the Alchemix Alchemist contract as well as the `fund_receiver` and add/remove other operators
    2) `is_depositor`: can deposit into the vault. In practice this will be the `AuctionHouse` contract.
    3) `migration_admin`: can set a migration contract and after 30 day timelock execute a migration. In practice this role 
will be handed over to the Alchemix Multisig and would only need to be used in case something significant changes at 
Alchemix. Since vault potentially holds an Alchemix position over a long time during which changes at Alchemix could happen, 
the `migration_admin` has complete control over the vault and its position after giving depositors a 30 day window to 
liquidate (or transfer with a flashloan) their position if they're not comfortable with the migration. `migration_admin` 
works under the same security and trust assumptions as the Alchemix (Proxy) Admins.

## `MintableERC721`:
    1) `owner`: one owner, in practice the `Vault` contract issuing a new token as receipt and control over a deposited 
position.


# Known Issues / Risks

During the auction phase all priviledged roles have to be trusted.  
Migration admin has to be trusted for the entire time, as long as there is an active position.  
Alchemix admins, protocol and underlying tokens have to be trusted.


# Contracts
- `AuctionHouse.vy` handles the daily auction
- `Vault.vy` handles the Alchemix integration
- `MintableERC721.sol` is an OpenZeppelin based ERC721 implementation used to represent the financial positions in the vault


# Tests
The Fair Funding platform has been developed almost exclusively test driven with the help of Titanoboa & Vyper.
A total of 133 tests cover all aspects of the auction, vault and user interactions.

We use Poetry (https://python-poetry.org/) for the python env management and Python version `3.10`.

To run the tests run:
```
poetry shell
poetry install
pytest
```

If `3.10` is not your default version and you're using `pyenv`, set up your environment first via:

```
pyenv install 3.10
pyenv local 3.10
poetry env use 3.10
poetry shell
poetry install
pytest
```


## Contact
https://unstoppable.ooo


## Security Contact
team@unstoppable.ooo
